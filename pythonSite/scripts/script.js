/*  Script de contrôle de l'interface */

let liMenu = document.getElementsByTagName('li')
console.log(liMenu)
let section = document.getElementsByTagName('section')
console.log(section)
let stop = document.getElementById('stop')
console.log(stop)

function actionMenu(numeroMenu) {

    liMenu[numeroMenu].addEventListener('click', function () {

        for (let i = 0; i < 4; i++) {
            if (i == numeroMenu) {
                section[i].style.display = "block"
            } else {
                section[i].style.display = "none"
            }
        }

    })
}

stop.addEventListener('click', function () {
    window.close()
})

actionMenu(0)
actionMenu(1)
actionMenu(2)
actionMenu(3)


